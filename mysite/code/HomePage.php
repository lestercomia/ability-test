<?php

/*
	model  It will contain all of the custom database fields, data relationships,
	 and functionality that can be expressed across multiple templates.
*/
class HomePage extends Page{

	private static $has_many = array(
			'HomeArticles' => 'Home',
		);

	 public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Articles', GridField::create(
            'HomeArticles',
            'Article on this page',
            $this->HomeArticles(),
            GridFieldConfig_RecordEditor::create()
        ));

        return $fields;
	}	
}
/*
The controller is the liaison between the HTTP request and the finalised template. 
Controllers can become very dense with functionality, 
and will commonly include functions for querying the database, 
handling form submissions, checking authentication, 
and dealing with an assortment of business logic.
*/
class HomePage_Controller extends Page_Controller{
	
	//method that display limit of 6 article on hompage
	//instantanctiate ORM static method from ArticlePage  
	//default display will be 6 item
	public function Artist($count = 6){
		return ArtistPage::get()
					->sort('Created', 'ASC')
					->limit($count);
	}


	public function Articles(){
		return Home::get()
					->sort('Created', 'DESC');
					
	}
	
}