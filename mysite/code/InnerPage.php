<?php

/*
	model  It will contain all of the custom database fields, data relationships,
	 and functionality that can be expressed across multiple templates.
*/
class InnerPage extends Page{

}
/*
The controller is the liaison between the HTTP request and the finalised template. 
Controllers can become very dense with functionality, 
and will commonly include functions for querying the database, 
handling form submissions, checking authentication, 
and dealing with an assortment of business logic.
*/
class InnerPage_Controller extends Page_Controller{
	

	public function Articles(){
		return Home::get()
					->sort('Created', 'DESC');
					
	}
	public function Artist(){
		return ArtistPage::get()
					->sort('Created', 'DESC');
					
	}
	
}