<?php

/*
	model  It will contain all of the custom database fields, data relationships,
	and functionality that can be expressed across multiple templates.
*/
class ArtistPage extends Page{
	private static $db = array(
			'Date' => 'Date',
			'ButtonName' => 'Varchar', 
			'ButtonLink' => 'Varchar'
		);
	private static $has_one = array(
			'Photo' => 'Image',

		);

	public function getCMSFields() {
   		 $fields = parent::getCMSFields();
   		
   		 $fields->addFieldToTab('Root.Main',DateField::create('Date','Date of Article' )
   		 	->setConfig('showcalendar', true), 'Content');
   		
   		 $fields->addFieldToTab('Root.Main',TextField::create('ButtonName','Button Name' )
   		 	->setDescription('Name of the button that will appear on the screen'), 'Content');
   		
   		 $fields->addFieldToTab('Root.Main',TextField::create('ButtonLink','Button Link' )
   		 	->setDescription('URL where the page will redirect to'), 'Content');

   		 //to tidyness put upload into its own tab
		 $fields->addFieldToTab('Root.Attachment',$photo = UploadField::create('Photo'));	
		 //validating upload photo
		 $photo->getValidator()->setAllowedExtensions(array('png','gif','jpg','jpeg'));
		 //adding specific location/folder of all uploaded photos
		 //if the folder not exist it will be created
		 $photo->setFolderName('Artist Photos');
    	
    	return $fields;
  	}


  	
}

/*
	The controller is the liaison between the HTTP request and the finalised template. 
	Controllers can become very dense with functionality, 
	and will commonly include functions for querying the database, 
	handling form submissions, checking authentication, 
	and dealing with an assortment of business logic.
*/
class ArtistPage_Controller extends Page_Controller{
	
	public function Articles(){
		return Home::get()
					->sort('Created', 'DESC');
					
	}
	
}