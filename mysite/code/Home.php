<?php 

class Home extends DataObject{
	private static $db = array(
			'Title' => 'Varchar',
			'Description' => 'Text',
			'Date' => 'Date',
			'ButtonName' => 'Varchar', 
			'ButtonLink' => 'Varchar'
		);
	private static $has_one = array(
			'Photo' => 'Image',
			'HomePage' => 'HomePage'

		);
	private static $summary_fields = array(
			'GridThumbnail' => '',
			'Title' => 'Title of Article',
			'Description' => 'Short description',
			'ButtonName' => 'Button Name',
			'ButtonLink' => 'Button Link',
		);
	public function getGridThumbnail(){
		if($this->Photo()->exists()){
			return $this->Photo()->setWidth(100);
		}
		return'(no image)';
	}


	public function getCMSFields() {
   		$fields = FieldList::create(
   				TextField::create('Title'),
   				TextareaField::create('Description'),
   				DateField::create('Date')
   					->setConfig('showcalendar', true),
   				TextField::create('ButtonName')
   					->setDescription('Name of the button that will appear on the screen'),
   				TextField::create('ButtonLink')
   					->setDescription('URL where the page will redirect to'),
   				$uploader = UploadField::create('Photo')
   			);
		//adding specific location/folder of all uploaded photos
		//if the folder not exist it will be created
		$uploader->setFolderName('Articles Photos');
		//validating upload photo
		$uploader->getValidator()->setAllowedExtensions(array('png','gif','jpg','jpeg'));
		
		return $fields;
  	}

}