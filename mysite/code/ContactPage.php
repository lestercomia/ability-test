
<?php
class ContactPage extends Page {

    static $db = array(
        'Mailto' => 'Varchar(100)',
        'SubmitText' => 'Text'
    );
     
    function getCMSFields() {
        $fields = parent::getCMSFields();
     
        $fields->addFieldToTab("Root.Content.OnSubmission", new TextField('Mailto', 'Email submissions to'));    
        $fields->addFieldToTab("Root.Content.OnSubmission", new TextareaField('SubmitText', 'Text on Submission'));      
     
        return $fields; 
    }
}
class ContactPage_Controller extends Page_Controller {

    private static $allowed_actions = array('ContactForm',);
   
    public function ContactForm() { 

        $form = Form::create(
            $this,
            __FUNCTION__,
            FieldList::create(
                TextField::create('Name','')
                    ->setAttribute('Placeholder','Name *')
                    ->addExtraClass('form-control'),
                EmailField::create('Email','')
                    ->setAttribute('Placeholder','Email *')
                    ->addExtraClass('form-control'),
                TextareaField::create('Comments','')
                    ->setAttribute('Placeholder','Comment *')
                    ->addExtraClass('form-control')
            ),
            FieldList::create(
                FormAction::create('SendContactForm','Send')
                ->setUseButtonTag(true)
                ->addExtraClass('btn btn-default')
            ),
            RequiredFields::create('Name','Email','Comments')

        );
        $form->addExtraClass('form-style');
        return $form;
    }



    function SendContactForm($data, $form) {
   
      
       $From = $data['Email'];
       // $To = $this->Mailto;
        $To = 'mcyodz16@gmail.com';
       $Subject = "Website Contact message";     
       $email = new Email($From, $To, $Subject);
        // set template
       $email->setTemplate('ContactEmail');
        // populate template
       $email->populateTemplate($data);
        // send mail
       $send = $email->send();
        // return to submitted message
        // Director::redirect($this->Link("?success=1"));
       if($send){
        return $this->redirect($this->Link("?success=1"));  
         // Director::redirect(Director::baseURL(). $this->URLSegment . "/?success=1");
        }
    }

       public function Success()
    {
        return isset($_REQUEST['success']) && $_REQUEST['success'] == "1";
    }






}


