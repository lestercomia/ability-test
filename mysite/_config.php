<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	'type' => 'MySQLDatabase',
	'server' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'SS_silverstripe',
	'path' => ''
);

// $databaseConfig = array(
// 	'type' => 'MySQLDatabase',
// 	'server' => 'pdb3.biz.nf',
// 	'username' => '1905035_silver',
// 	'password' => 'silver12345',
// 	'database' => '1905035_silver',
// 	'path' => ''
// );

// Set the site locale
i18n::set_locale('en_US');
